بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم

Sodaqh kami rencanakan menjadi sebuah ruang beramal non-harta (shodaqoh) crowd-sourcing di dunia maya.

Proyek diinisialisasi Jumat Kliwon, 19 Juli 2019 (16 Dzulqaidah 1440).

### Lisensi

Sodaqh merupakan perangkat lunak sumber-terbuka dalam lisensi [MIT license](https://opensource.org/licenses/MIT).